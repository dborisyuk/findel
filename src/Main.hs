{-# LANGUAGE FlexibleContexts #-}

import qualified System.IO.UTF8 as S
import System.Environment
import Data.List (sortBy, elemIndex)
import Data.Function (on)

import Data.Array (elems)
import Data.Array.ST (runSTArray)
import Data.Array.MArray (newListArray, writeArray, readArray, Ix, MArray)


data Counter = Counter {
    symbol :: Char,
    count :: Int
} deriving (Eq, Show)


main :: IO ()
main = do
  args <- getArgs
  let filename = parseParam "-f" args
  text <-
    case filename of
      Nothing       -> fail "No file path was set"
      Just filename -> S.readFile $ read $ show filename
  let counted = foldl (\x y -> modifyArrayL x (getElem x y)) [] text
--  let counted = foldr modifyArrayR [] text -- using foldr
  let sorted = sortBy (flip compare `on` count) counted
  putStrLn $ unlines $ printtuple sorted



getElem :: [Counter] -> Char -> Counter
getElem xs a = do
  let c = filter (\x -> symbol x == a) xs
  if length c == 0  -- if null c - lazy
    then Counter{symbol = a, count = 1}
    else head c


currCounter :: Counter -> Counter
currCounter a = a{count = c}
  where c = count a + 1

-- using foldl
modifyArrayL :: [Counter] -> Counter -> [Counter]
modifyArrayL [] c = [c{count = 1}]
modifyArrayL xs c = elems $ runSTArray $ modifyArray' xs i c
  where i = elemIndex c xs


-- using foldr
modifyArrayR :: Char -> [Counter] -> [Counter]
modifyArrayR a [] = [Counter{symbol = a, count = 1}]
modifyArrayR a xs = elems $ runSTArray $ modifyArray' xs i c
  where c = getElem xs a
        i = elemIndex c xs


modifyArray' :: MArray a Counter m => [Counter] -> Maybe Int -> Counter -> m (a Int Counter)
modifyArray' xs Nothing c = do
  arr <- newListArray (0, length xs) xs
  writeArray arr (length xs) c
  return arr
modifyArray' xs (Just i) c = do
  arr <- newListArray (0, length xs - 1) xs
  modifyArraySwap arr i (currCounter c)
  return arr


modifyArraySwap :: (Num i, Ix i, MArray a Counter m) => a i Counter -> i -> Counter -> m ()
modifyArraySwap arr 0 next = writeArray arr 0 next
modifyArraySwap arr i next = do
  next' <- readArray arr (i - 1)
  if count next > count next'
    then do
      writeArray arr i next'
      writeArray arr (i - 1) next
    else writeArray arr i next
  


printtuple:: [Counter] -> [String]
printtuple = map line
  where line x = (case symbol x of
                        '\r' -> "\\r"
                        '\n' -> "\\n"
                        _    -> [symbol x]) ++ " : " ++ show (count x)


parseParam :: String -> [String] -> Maybe String
parseParam "-f" [] = Just "text/google.ua.work"
parseParam _ []        = Nothing
parseParam "-f" ["-f"] = Nothing
parseParam "" _        = Nothing
parseParam k (x:xs)    =
    case x of 
      "-f" -> Just $ head xs
      _    -> parseParam k xs
